#!/usr/bin/env bash


brew tap codekitchen/dinghy

brew install dinghy

brew install docker docker-machine

dinghy create --provider virtualbox