#!/usr/bin/env bash
git submodule init
git submodule add https://github.com/kkoojjyy/install-docker.git
git submodule add https://github.com/kkoojjyy/dinghy.git
git submodule add https://github.com/machine-drivers/docker-machine-driver-xhyve.git
git submodule add https://github.com/kkoojjyy/docker-machine-nfs.git
git submodule add https://github.com/dhiltgen/docker-machine-kvm.git
git submodule add https://github.com/StefanScherer/windows-docker-machine.git
git submodule add https://github.com/kkoojjyy/machine-share.git
git submodule add https://github.com/kkoojjyy/kmachine.git
git submodule add https://github.com/kkoojjyy/vm.git
git submodule add https://github.com/kkoojjyy/portainer.git
#反向dns代理
git submodule add https://github.com/kkoojjyy/muguet.git

#啊里
git submodule add https://github.com/AliyunContainerService/docker-machine-driver-aliyunecs.git


git submodule update
